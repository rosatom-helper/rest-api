<p style="text-align: center;">
  <a href="https://www.rosatom.ru/" target="blank"><img src="https://seeklogo.com/images/R/rosatom-logo-3EA6526E29-seeklogo.com.png" width="100" alt="Rosatom Logo" /></a>
</p>

## Description

[Nest](https://github.com/nestjs/nest) application for handling Rosatom-helper android app with DialogFlow integration.
Was done with love by BORN2CODE on the [leaders-of-digital](https://leadersofdigital.ru/) contest.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run build
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
