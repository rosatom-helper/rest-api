import {
  Inject,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { Repository, ILike, MoreThanOrEqual } from 'typeorm';
import { TaskFilterDto } from '../dto/task-filter.dto';
import { TASK_REPOSITORY, SECTOR_REPOSITORY } from '../consts/database.const';
import { Task } from '../entities/task.entity';
import { Sector } from '../entities/sector.entity';
import { TaskDto } from '../dto/task.dto';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class TaskService {
  constructor(
    @Inject(TASK_REPOSITORY)
    private readonly taskRepository: Repository<Task>,
    @Inject(SECTOR_REPOSITORY)
    private readonly sectorRepository: Repository<Sector>,
    private readonly userService: UsersService,
  ) {}

  async getTasks(
    taskFilterDto: TaskFilterDto,
  ): Promise<{ tasks: Task[]; total: number }> {
    const options: { [key: string]: any } = {
      where: {},
      skip: taskFilterDto.offset,
      take: taskFilterDto.limit,
      order: {
        createdAt: taskFilterDto.sort,
      },
      relations: ['sector'],
    };

    if (Object.prototype.hasOwnProperty.call(taskFilterDto, 'isHighPriority')) {
      options.where.isHighPriority = taskFilterDto.isHighPriority;
    }

    if (Object.prototype.hasOwnProperty.call(taskFilterDto, 'isDone')) {
      options.where.isDone = taskFilterDto.isDone;
    }

    if (Object.prototype.hasOwnProperty.call(taskFilterDto, 'isForAll')) {
      options.where.isForAll = taskFilterDto.isForAll;
    }

    if (Object.prototype.hasOwnProperty.call(taskFilterDto, 'isComments')) {
      options.where.commentsCount = taskFilterDto.isComments
        ? MoreThanOrEqual(1)
        : 0;
    }

    if (Object.prototype.hasOwnProperty.call(taskFilterDto, 'isAttachments')) {
      options.where.attachmentCount = taskFilterDto.isAttachments
        ? MoreThanOrEqual(1)
        : 0;
    }

    if (taskFilterDto.search) {
      options.where.name = ILike(`%${taskFilterDto.search}%`);
      options.where.description = ILike(`%${taskFilterDto.search}%`);
    }

    if (taskFilterDto.status) {
      options.where.status = taskFilterDto.status;
    }

    if (taskFilterDto.type) {
      options.where.type = taskFilterDto.type;
    }

    try {
      const [tasks, total] = await this.taskRepository.findAndCount(options);
      return { tasks, total };
    } catch (e) {
      return {
        tasks: [],
        total: 0,
      };
    }
  }

  async getTask(id: string): Promise<Task> {
    let task: Task = null;
    try {
      task = await this.taskRepository.findOne(id, {
        relations: ['sector'],
      });
    } catch (e) {
      throw new InternalServerErrorException(e.message);
    }
    if (!task) {
      throw new NotFoundException('Task not found.');
    }
    return task;
  }

  async createTask(taskDto: TaskDto): Promise<Task> {
    const { sectorId, comments, authorId, executorsIds, ...taskData } = taskDto;
    let task: Task = null;

    try {
      task = this.taskRepository.create(taskData);
      await this.taskRepository.save(task);
    } catch (e) {
      throw new InternalServerErrorException(e.message);
    }

    if (sectorId) {
      try {
        const sector = await this.sectorRepository.findOne(sectorId);
        task.sector = sector;
        await this.taskRepository.save(task);
      } catch (e) {
        throw new InternalServerErrorException(e.message);
      }
    }

    if (authorId) {
      try {
        const author = await this.userService.findOne(authorId);
        task.author = author;
        await this.taskRepository.save(task);
      } catch (e) {
        throw new InternalServerErrorException(e.message);
      }
    }

    if (executorsIds) {
      try {
        const executors = await this.userService.findByIds(executorsIds);
        task.executors = executors;
        await this.taskRepository.save(task);
      } catch (e) {
        throw new InternalServerErrorException(e.message);
      }
    }

    if (comments && !taskDto.commentsCount) {
      task.commentsCount = comments.length;
      await this.taskRepository.save(task);
    }

    return task;
  }

  async updateTask(taskId: string, updatedTaskDto: TaskDto) {
    const {
      sectorId,
      comments,
      authorId,
      executorsIds,
      ...taskData
    } = updatedTaskDto;
    let task: Task = null;

    try {
      task = await this.getTask(taskId);
    } catch (e) {
      throw e;
    }

    try {
      await this.taskRepository.update(taskId, taskData);
    } catch (e) {
      throw new InternalServerErrorException(e.message);
    }

    if (sectorId) {
      try {
        const sector = await this.sectorRepository.findOne(sectorId);
        task.sector = sector;
        await this.taskRepository.save(task);
      } catch (e) {
        throw new InternalServerErrorException(e.message);
      }
    }

    if (authorId) {
      try {
        const author = await this.userService.findOne(authorId);
        task.author = author;
        await this.taskRepository.save(task);
      } catch (e) {
        throw new InternalServerErrorException(e.message);
      }
    }

    if (executorsIds) {
      try {
        const executors = await this.userService.findByIds(executorsIds);
        task.executors = executors;
        await this.taskRepository.save(task);
      } catch (e) {
        throw new InternalServerErrorException(e.message);
      }
    }

    if (comments) {
      task.commentsCount = comments.length;
      await this.taskRepository.save(task);
    }

    return task;
  }

  async setHighPriority(id: string) {
    return this.updateTask(id, { isHighPriority: true });
  }

  async removeHighPriority(id: string) {
    return this.updateTask(id, { isHighPriority: false });
  }

  async completeTask(id: string) {
    return this.updateTask(id, { isDone: true });
  }

  async openTask(id: string) {
    return this.updateTask(id, { isDone: false });
  }

  async setStatus(id: string, status: string) {
    return this.updateTask(id, { status });
  }

  async addTag(id: string, tag: string) {
    let task: Task = null;

    try {
      task = await this.getTask(id);
    } catch (e) {
      throw e;
    }

    const tags = new Set(task.tags);
    tags.add(tag);

    return this.updateTask(id, { tags: [...tags] });
  }

  async deleteTask(taskId: string) {
    let task;

    try {
      task = await this.getTask(taskId);
    } catch (e) {
      throw e;
    }

    try {
      await this.taskRepository.remove(task);
    } catch (e) {
      throw new InternalServerErrorException(e.message);
    }

    return;
  }
}
