import {
  Inject,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { Repository, ILike } from 'typeorm';
import { TASK_REPOSITORY, SECTOR_REPOSITORY } from '../consts/database.const';
import { Task } from '../entities/task.entity';
import { Sector } from '../entities/sector.entity';
import { SectorFilterDto } from '../dto/sector-filter.dto';
import { SectorDto } from '../dto/sector.dto';

@Injectable()
export class SectorService {
  constructor(
    @Inject(TASK_REPOSITORY)
    private readonly taskRepository: Repository<Task>,
    @Inject(SECTOR_REPOSITORY)
    private readonly sectorRepository: Repository<Sector>,
  ) {}

  async getSectors(
    sectorFilterDto: SectorFilterDto,
  ): Promise<{ sectors: Sector[]; total: number }> {
    const options: { [key: string]: any } = {
      where: {},
      skip: sectorFilterDto.offset,
      take: sectorFilterDto.limit,
      order: {
        createdAt: sectorFilterDto.sort,
      },
      relations: ['tasks'],
    };

    if (sectorFilterDto.search) {
      options.where.name = ILike(`%${sectorFilterDto.search}%`);
      options.where.description = ILike(`%${sectorFilterDto.search}%`);
    }

    try {
      const [sectors, total] = await this.sectorRepository.findAndCount(
        options,
      );
      return { sectors, total };
    } catch (e) {
      return {
        sectors: [],
        total: 0,
      };
    }
  }

  async getSector(id: string): Promise<Sector> {
    let sector: Sector = null;
    try {
      sector = await this.sectorRepository.findOne(id, {
        relations: ['tasks'],
      });
    } catch (e) {
      throw new InternalServerErrorException(e.message);
    }
    if (!sector) {
      throw new NotFoundException('Sector not found.');
    }
    return sector;
  }

  async createSector(sectorDto: SectorDto): Promise<Sector> {
    const { tasksIds, ...sectorData } = sectorDto;
    let sector: Sector = null;

    try {
      sector = this.sectorRepository.create(sectorData);
      await this.sectorRepository.save(sector);
    } catch (e) {
      throw new InternalServerErrorException(e.message);
    }

    if (tasksIds) {
      try {
        const tasks = await this.taskRepository.findByIds(tasksIds);
        sector.tasks = tasks;
        await this.sectorRepository.save(sector);
      } catch (e) {
        throw new InternalServerErrorException(e.message);
      }
    }

    return sector;
  }

  async updateSector(sectorId: string, updatedSectorDto: SectorDto) {
    const { tasksIds, ...sectorData } = updatedSectorDto;
    let sector: Sector = null;

    try {
      sector = await this.getSector(sectorId);
    } catch (e) {
      throw e;
    }

    try {
      await this.sectorRepository.update(sectorId, sectorData);
    } catch (e) {
      throw new InternalServerErrorException(e.message);
    }

    if (tasksIds) {
      try {
        const tasks = await this.taskRepository.findByIds(tasksIds);
        sector.tasks = tasks;
        await this.sectorRepository.save(sector);
      } catch (e) {
        throw new InternalServerErrorException(e.message);
      }
    }

    return sector;
  }

  async addTask(sectorId: string, taskId: string) {
    let sector: Sector = null;

    try {
      sector = await this.getSector(sectorId);
    } catch (e) {
      throw e;
    }

    const tasks = new Set(sector.tasks.map(({ id }) => id));
    tasks.add(taskId);

    return this.updateSector(sectorId, { tasksIds: [...tasks] });
  }

  async deleteSector(sectorId: string) {
    let sector;

    try {
      sector = await this.getSector(sectorId);
    } catch (e) {
      throw e;
    }

    try {
      await this.sectorRepository.remove(sector);
    } catch (e) {
      throw new InternalServerErrorException(e.message);
    }

    return;
  }
}
