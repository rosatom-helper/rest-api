import { IsOptional, IsString, IsUUID, IsDate } from 'class-validator';
import { Type } from 'class-transformer';

export class CommentDto {
  @IsOptional()
  @IsUUID()
  userId: string;

  @IsOptional()
  @IsString()
  message: string;

  @IsOptional()
  @IsDate()
  @Type(() => Date)
  createdAt: Date;

  @IsOptional()
  @IsDate()
  @Type(() => Date)
  updatedAt: Date;
}
