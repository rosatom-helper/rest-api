import { IsString, MaxLength } from 'class-validator';
import { Transform } from 'class-transformer';

export class TagItemDto {
  @Transform((value) => value.trim())
  @IsString()
  @MaxLength(255)
  tag: string;
}
