import {
  IsOptional,
  IsString,
  IsIn,
  IsBoolean,
  Matches,
  MaxLength,
} from 'class-validator';
import { Transform } from 'class-transformer';
import { ASC, DESC } from '@core/consts/sort.const';
import { PaginationDto } from '@core/dto/pagination.dto';

const transformBoolean = (value) => ['true', '1', true, 1].includes(value);

export class TaskFilterDto extends PaginationDto {
  @IsOptional()
  @Transform(transformBoolean)
  @IsBoolean()
  isHighPriority: boolean;

  @IsOptional()
  @Transform(transformBoolean)
  @IsBoolean()
  isDone: boolean;

  @IsOptional()
  @Transform(transformBoolean)
  @IsBoolean()
  isForAll: boolean;

  @IsOptional()
  @Transform(transformBoolean)
  @IsBoolean()
  isComments: boolean;

  @IsOptional()
  @Transform(transformBoolean)
  @IsBoolean()
  isAttachments: boolean;

  @IsOptional()
  @Transform((value) => value.trim())
  @IsString()
  @MaxLength(255)
  @Matches(/^[a-z-_]{1,255}$/i)
  status: string;

  @IsOptional()
  @Transform((value) => value.trim())
  @IsString()
  @MaxLength(255)
  @Matches(/^[a-z-_]{1,255}$/i)
  type: string;

  @IsOptional()
  @Transform((value) => value.trim())
  @IsString()
  @MaxLength(255)
  @Matches(/^[a-z0-9- _]{1,255}$/i)
  search: string;

  @IsOptional()
  @IsString()
  @IsIn([ASC, DESC])
  sort: string = DESC;
}
