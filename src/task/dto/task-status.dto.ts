import { IsString, MaxLength, Matches } from 'class-validator';
import { Transform } from 'class-transformer';

export class StatusDto {
  @Transform((value) => value.trim())
  @IsString()
  @MaxLength(255)
  @Matches(/^[a-z-_]{1,255}$/i)
  status: string;
}
