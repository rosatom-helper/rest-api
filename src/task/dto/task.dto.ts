import {
  IsOptional,
  IsString,
  IsBoolean,
  IsDate,
  IsInt,
  IsObject,
  Matches,
  MaxLength,
  Min,
  ValidateNested,
  IsUUID,
} from 'class-validator';
import { Transform, Type } from 'class-transformer';
import { ObjectLiteral } from 'typeorm';
import { CommentDto } from './comment.dto';

const transformBoolean = (value) => ['true', '1', true, 1].includes(value);

export class TaskDto {
  @IsOptional()
  @Transform((value) => value.trim())
  @IsString()
  @Matches(/^[a-z0-9- _]{1,255}$/i)
  name?: string;

  @IsOptional()
  @IsDate()
  @Type(() => Date)
  endDate?: Date;

  @IsOptional()
  @Transform((value) => value.trim())
  @IsString()
  description?: string;

  @IsOptional()
  @Transform(transformBoolean)
  @IsBoolean()
  isHighPriority?: boolean;

  @IsOptional()
  @Transform(transformBoolean)
  @IsBoolean()
  isDone?: boolean;

  @IsOptional()
  @Transform((value) => value.trim())
  @IsString()
  @MaxLength(255)
  @Matches(/^[a-z-_]{1,255}$/i)
  status?: string;

  @IsOptional()
  @IsInt()
  @Min(0)
  commentsCount?: number;

  @IsOptional()
  @IsInt()
  @Min(0)
  attachmentCount?: number;

  @IsOptional()
  @IsString({ each: true })
  tags?: string[];

  @IsOptional()
  @IsObject()
  metadata?: ObjectLiteral;

  @IsOptional()
  @ValidateNested({ each: true })
  @Type(() => CommentDto)
  comments?: ObjectLiteral[];

  @IsOptional()
  @Transform((value) => value.trim())
  @IsString()
  @MaxLength(255)
  @Matches(/^[a-z-_]{1,255}$/i)
  type?: string;

  @IsOptional()
  @Transform(transformBoolean)
  @IsBoolean()
  isForAll?: boolean;

  @IsOptional()
  @IsUUID()
  sectorId?: string;

  @IsOptional()
  @IsUUID()
  authorId?: string;

  @IsOptional()
  @IsUUID(4, { each: true })
  executorsIds?: string[];
}
