import {
  IsOptional,
  IsString,
  IsObject,
  Matches,
  IsUUID,
} from 'class-validator';
import { Transform } from 'class-transformer';
import { ObjectLiteral } from 'typeorm';

export class SectorDto {
  @IsOptional()
  @Transform((value) => value.trim())
  @IsString()
  @Matches(/^[a-z0-9- _]{1,255}$/i)
  name?: string;

  @IsOptional()
  @Transform((value) => value.trim())
  @IsString()
  description?: string;

  @IsOptional()
  @IsObject()
  metadata?: ObjectLiteral;

  @IsOptional()
  @IsUUID(4, { each: true })
  tasksIds?: string[];
}
