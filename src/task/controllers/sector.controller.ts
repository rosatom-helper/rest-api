import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  Query,
  SerializeOptions,
  UseInterceptors,
} from '@nestjs/common';
import { Permissions } from '@acl/decorators';
import { PERMISSION } from '@acl/consts/permissions.const';
import { Task } from '../entities/task.entity';
import { SectorService } from '../services/sector.service';
import { SectorFilterDto } from '../dto/sector-filter.dto';
import { Sector } from '../entities/sector.entity';
import { SectorDto } from '../dto/sector.dto';
import { TaskIdDto } from '../dto/task-id.dto';

@Controller('sectors')
@UseInterceptors(ClassSerializerInterceptor)
export class SectorController {
  constructor(private sectorService: SectorService) {}

  @Get()
  @SerializeOptions({
    groups: ['default'],
  })
  @Permissions(PERMISSION.SECTOR_VIEW)
  async getSectors(
    @Query() params: SectorFilterDto,
  ): Promise<{ sectors: Sector[]; total: number }> {
    return this.sectorService.getSectors(params);
  }

  @Get(':id')
  @SerializeOptions({
    groups: ['default'],
  })
  @Permissions(PERMISSION.SECTOR_VIEW)
  async getSector(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.sectorService.getSector(id);
  }

  @Post()
  @SerializeOptions({
    groups: ['default'],
  })
  @Permissions(PERMISSION.SECTOR_ADD)
  async createSector(@Body() sectorDto: SectorDto): Promise<Partial<Sector>> {
    return this.sectorService.createSector(sectorDto);
  }

  @Patch(':id')
  @SerializeOptions({
    groups: ['default'],
  })
  @Permissions(PERMISSION.SECTOR_EDIT)
  async updateSector(
    @Param('id', new ParseUUIDPipe()) id: string,
    @Body() sectorDto: SectorDto,
  ): Promise<Partial<Sector>> {
    return this.sectorService.updateSector(id, sectorDto);
  }

  @Delete(':id')
  @Permissions(PERMISSION.SECTOR_DELETE)
  async deleteRole(
    @Param('id', new ParseUUIDPipe()) id: string,
  ): Promise<void> {
    return this.sectorService.deleteSector(id);
  }

  @Patch(':id/add-task')
  @SerializeOptions({
    groups: ['default'],
  })
  @Permissions(PERMISSION.SECTOR_EDIT)
  async addTask(
    @Param('id', new ParseUUIDPipe()) id: string,
    @Body() taskIdDto: TaskIdDto,
  ): Promise<Partial<Task>> {
    return this.sectorService.addTask(id, taskIdDto.taskId);
  }
}
