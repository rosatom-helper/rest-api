import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  Query,
  SerializeOptions,
  UseInterceptors,
} from '@nestjs/common';
import { Permissions } from '@acl/decorators';
import { PERMISSION } from '@acl/consts/permissions.const';
import { TaskService } from '../services/task.service';
import { TaskFilterDto } from '../dto/task-filter.dto';
import { Task } from '../entities/task.entity';
import { TaskDto } from '../dto/task.dto';
import { StatusDto } from '../dto/task-status.dto';
import { TagItemDto } from '../dto/task-tag-item.dto';

@Controller('tasks')
@UseInterceptors(ClassSerializerInterceptor)
export class TaskController {
  constructor(private taskService: TaskService) {}

  @Get()
  @SerializeOptions({
    groups: ['default'],
  })
  @Permissions(PERMISSION.TASKS_VIEW)
  async getTasks(
    @Query() params: TaskFilterDto,
  ): Promise<{ tasks: Task[]; total: number }> {
    return this.taskService.getTasks(params);
  }

  @Get(':id')
  @SerializeOptions({
    groups: ['default'],
  })
  @Permissions(PERMISSION.TASKS_VIEW)
  async getTask(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.taskService.getTask(id);
  }

  @Post()
  @SerializeOptions({
    groups: ['default'],
  })
  @Permissions(PERMISSION.TASKS_ADD)
  async createTask(@Body() taskDto: TaskDto): Promise<Partial<Task>> {
    return this.taskService.createTask(taskDto);
  }

  @Patch(':id')
  @SerializeOptions({
    groups: ['default'],
  })
  @Permissions(PERMISSION.TASKS_EDIT)
  async updateTask(
    @Param('id', new ParseUUIDPipe()) id: string,
    @Body() taskDto: TaskDto,
  ): Promise<Partial<Task>> {
    return this.taskService.updateTask(id, taskDto);
  }

  @Delete(':id')
  @Permissions(PERMISSION.TASKS_DELETE)
  async deleteRole(
    @Param('id', new ParseUUIDPipe()) id: string,
  ): Promise<void> {
    return this.taskService.deleteTask(id);
  }

  @Patch(':id/set-high-priority')
  @Permissions(PERMISSION.TASKS_EDIT)
  async setHighPriority(
    @Param('id', new ParseUUIDPipe()) id: string,
  ): Promise<void> {
    await this.taskService.setHighPriority(id);
    return;
  }

  @Patch(':id/remove-high-priority')
  @Permissions(PERMISSION.TASKS_EDIT)
  async removeHighPriority(
    @Param('id', new ParseUUIDPipe()) id: string,
  ): Promise<void> {
    await this.taskService.removeHighPriority(id);
    return;
  }

  @Patch(':id/complete-task')
  @Permissions(PERMISSION.TASKS_EDIT)
  async completeTask(
    @Param('id', new ParseUUIDPipe()) id: string,
  ): Promise<void> {
    await this.taskService.completeTask(id);
    return;
  }

  @Patch(':id/open-task')
  @Permissions(PERMISSION.TASKS_EDIT)
  async openTask(@Param('id', new ParseUUIDPipe()) id: string): Promise<void> {
    await this.taskService.openTask(id);
    return;
  }

  @Patch(':id/set-status')
  @SerializeOptions({
    groups: ['default'],
  })
  @Permissions(PERMISSION.TASKS_EDIT)
  async setStatus(
    @Param('id', new ParseUUIDPipe()) id: string,
    @Body() statusDto: StatusDto,
  ): Promise<Partial<Task>> {
    return this.taskService.setStatus(id, statusDto.status);
  }

  @Patch(':id/add-tag')
  @SerializeOptions({
    groups: ['default'],
  })
  @Permissions(PERMISSION.TASKS_EDIT)
  async addTag(
    @Param('id', new ParseUUIDPipe()) id: string,
    @Body() tagItemDto: TagItemDto,
  ): Promise<Partial<Task>> {
    return this.taskService.addTag(id, tagItemDto.tag);
  }
}
