import { Global, Module } from '@nestjs/common';
import { taskProviders } from './task.providers';
import { TaskService } from './services/task.service';
import { SectorService } from './services/sector.service';
import { TaskController } from './controllers/task.controller';
import { SectorController } from './controllers/sector.controller';

import { UsersModule } from '@user/users.module';
import { UsersService } from '@user/users.service';

@Global()
@Module({
  imports: [UsersModule],
  providers: [TaskService, SectorService, ...taskProviders],
  exports: [TaskService, SectorService, ...taskProviders],
  controllers: [TaskController, SectorController],
})
export class TaskModule {}
