import { Connection } from 'typeorm';
import { DATABASE_CONNECTION } from '@core/database/consts/common.const';
import { Task } from './entities/task.entity';
import { Sector } from './entities/sector.entity';
import { TASK_REPOSITORY, SECTOR_REPOSITORY } from './consts/database.const';

export const taskProviders = [
  {
    provide: TASK_REPOSITORY,
    useFactory: (connection: Connection) => {
      return connection.getRepository(Task);
    },
    inject: [DATABASE_CONNECTION],
  },
  {
    provide: SECTOR_REPOSITORY,
    useFactory: (connection: Connection) => {
      return connection.getRepository(Sector);
    },
    inject: [DATABASE_CONNECTION],
  },
];
