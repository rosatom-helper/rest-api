import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  ObjectLiteral,
} from 'typeorm';
import { Exclude, Expose } from 'class-transformer';
import { Task } from './task.entity';

@Exclude()
@Entity('sectors')
export class Sector {
  @Expose({ groups: ['default'] })
  @PrimaryGeneratedColumn('uuid')
  readonly id: string;

  @Expose({ groups: ['default'] })
  @Column({
    type: 'text',
    nullable: true,
  })
  name: string;

  @Expose({ groups: ['default'] })
  @Column({
    type: 'text',
    nullable: true,
  })
  description: string;

  @Expose({ groups: ['default'] })
  @Column('jsonb')
  metadata: ObjectLiteral;

  @Expose({ groups: ['default', 'tasks'] })
  @OneToMany(() => Task, (task) => task.sector)
  tasks: Task[];

  @Expose({ groups: ['default'] })
  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @Expose({ groups: ['default'] })
  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;
}
