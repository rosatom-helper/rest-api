import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  ObjectLiteral,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { Exclude, Expose } from 'class-transformer';
import { Sector } from './sector.entity';
import { User } from 'src/users/user.entity';

@Exclude()
@Entity('tasks')
export class Task {
  @Expose({ groups: ['default'] })
  @PrimaryGeneratedColumn('uuid')
  readonly id: string;

  @Expose({ groups: ['default'] })
  @Column({
    type: 'text',
    nullable: true,
  })
  name: string;

  @Expose({ groups: ['default'] })
  @Column({
    name: 'end_date',
    type: 'timestamptz',
  })
  endDate: Date;

  @Expose({ groups: ['default'] })
  @Column({
    type: 'text',
    nullable: true,
  })
  description: string;

  @Expose({ groups: ['default'] })
  @Column({
    name: 'is_high_priority',
    type: 'boolean',
    default: false,
  })
  isHighPriority: boolean;

  @Expose({ groups: ['default'] })
  @Column({
    name: 'is_done',
    type: 'boolean',
    default: false,
  })
  isDone: boolean;

  @Column({
    type: 'varchar',
    length: 255,
  })
  status: string;

  @Expose({ groups: ['default'] })
  @Column({
    name: 'comments_count',
    type: 'int',
    default: 0,
  })
  commentsCount: number;

  @Expose({ groups: ['default'] })
  @Column({
    name: 'attachment_count',
    type: 'int',
    default: 0,
  })
  attachmentCount: number;

  @Expose({ groups: ['default'] })
  @Column('jsonb')
  tags: string[];

  @Expose({ groups: ['default'] })
  @Column('jsonb')
  metadata: ObjectLiteral;

  @Expose({ groups: ['default'] })
  @Column('jsonb')
  comments: ObjectLiteral[];

  @Expose({ groups: ['default'] })
  @Column({
    type: 'varchar',
    length: 255,
  })
  type: string;

  @Expose({ groups: ['default'] })
  @Column({
    name: 'is_for_all',
    type: 'boolean',
    default: false,
  })
  isForAll: boolean;

  @Expose({ groups: ['default', 'sector'] })
  @ManyToOne(() => Sector, (sector) => sector.tasks)
  sector: Sector;

  @Expose({ groups: ['default', 'users'] })
  @ManyToOne(() => User, (user) => user.orders)
  author: User;

  @Expose({ groups: ['default', 'users'] })
  @ManyToMany(() => User, (user) => user.tasks)
  @JoinTable({
    name: 'users_tasks',
    joinColumn: { name: 'task_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'user_id', referencedColumnName: 'id' },
  })
  executors: User[];

  @Expose({ groups: ['default'] })
  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @Expose({ groups: ['default'] })
  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;
}
