import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { config } from 'dotenv';
import { join } from 'path';

config();

class ConfigService {
  constructor(private env: { [k: string]: string | undefined }) {}

  private getValue(key: string, throwOnMissing = true): string {
    const value = this.env[key];
    if (!value && throwOnMissing) {
      throw new Error(`config error - missing env.${key}`);
    }

    return value;
  }

  public ensureValues(keys: string[]) {
    keys.forEach((k) => this.getValue(k, true));
    return this;
  }

  public getPort() {
    return this.getValue('PORT', true);
  }

  public isProduction() {
    const mode = this.getValue('MODE', false);
    return mode === 'PRODUCTION';
  }

  public getTypeOrmConfig(): TypeOrmModuleOptions {
    return {
      type: 'postgres',

      host: this.isProduction()
        ? `/cloudsql/${this.getValue('INSTANCE_CONNECTION_NAME')}`
        : 'localhost',
      username: this.getValue('SQL_USER'),
      password: this.getValue('SQL_PASSWORD'),
      database: this.getValue('SQL_DATABASE'),
      extra: {
        socketPath: this.isProduction()
          ? `/cloudsql/${this.getValue('INSTANCE_CONNECTION_NAME')}`
          : undefined,
      },

      entities: [join(__dirname, '**', '*.entity.{ts,js}')],

      migrationsTableName: 'migration',

      migrations: ['dist/migration/**/*.js'],

      cli: {
        migrationsDir: 'src/migration',
      },
      autoLoadEntities: true,
      synchronize: true,
    };
  }
}

const configService = new ConfigService(process.env).ensureValues([
  'SQL_USER',
  'SQL_PASSWORD',
  'SQL_DATABASE',
]);

export { configService };
