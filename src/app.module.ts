import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { configService } from './config.service';
import { DatabaseModule } from '@core/database/database.module';
import { AclModule } from '@acl/acl.module';
import { CoreModule } from '@core/core.module';
import { UsersModule } from './users/users.module';
import { DialogFlowModule } from 'nestjs-dialogflow';
import { DialogFlowProvider } from './dialogflow/providers/dialogflow.provider';
import { TaskModule } from '@task/task.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
    DatabaseModule,
    DialogFlowModule.forRoot({
      basePath: 'web-hooks',
      postPath: 'dialog-flow',
    }),
    AclModule,
    CoreModule,
    TaskModule,
    UsersModule,
  ],
  controllers: [AppController],
  providers: [AppService, DialogFlowProvider],
})
export class AppModule {}
