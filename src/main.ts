import { NestFactory, Reflector } from '@nestjs/core';
import { AppModule } from './app.module';
import { HttpExceptionFilter } from '@core/filters/http-exception.filter';
// import { getRepository } from 'typeorm';
// import { RoleGuard } from '@acl/guards/role.guard';
// import { PermissionGuard } from '@acl/guards/permission.guard';
// import { AclService } from '@acl/services/acl.service';
// import { Role } from '@acl/entities/role.entity';
// import { Permission } from '@acl/entities/permission.entity';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalFilters(new HttpExceptionFilter());
  // app.useGlobalGuards(
  //   new AuthGuard(app.get(Reflector)),
  //   new RoleGuard(app.get(Reflector)),
  //   new PermissionGuard(
  //     app.get(Reflector),
  //     new AclService(getRepository(Role), getRepository(Permission)),
  //   ),
  // );

  await app.listen(8080);
}
bootstrap();
