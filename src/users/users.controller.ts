import { Controller, Get, Post, Req } from '@nestjs/common';
import { Request } from 'express';
import { User } from './user.entity';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private readonly UsersService: UsersService) {}

  @Get()
  async getUsers(): Promise<User[]> {
    return await this.UsersService.findAll();
  }
  @Post()
  async createUser(@Req() request: Request): Promise<User> {
    return await this.UsersService.add(request.body);
  }
}
