import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToMany,
} from 'typeorm';
import { Exclude, Expose } from 'class-transformer';
import { Task } from '@task/entities/task.entity';

@Exclude()
@Entity()
export class User {
  @Expose({ groups: ['default'] })
  @PrimaryGeneratedColumn()
  id: number;

  @Expose({ groups: ['default'] })
  @Column()
  firstName: string;

  @Expose({ groups: ['default'] })
  @Column()
  lastName: string;

  @Expose({ groups: ['default', 'tasks'] })
  @OneToMany(() => Task, (task) => task.author)
  orders: Task[];
  @Expose({ groups: ['default', 'tasks'] })
  @ManyToMany(() => Task, (task) => task.executors)
  tasks: Task[];

  @Expose({ groups: ['default'] })
  @Column({ default: true })
  isActive: boolean;
}
