import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async findAll(): Promise<User[]> {
    return this.usersRepository.find();
  }

  async findOne(id: string): Promise<User> {
    return this.usersRepository.findOne(id);
  }

  async findByIds(ids: string[]): Promise<User[]> {
    return this.usersRepository.findByIds(ids);
  }
  async findByName(name: string): Promise<User> | null {
    const users = await this.usersRepository.find({
      where: { lastName: name },
    });
    if (users && users.length) {
      return users[0];
    } else {
      return null;
    }
  }

  async remove(id: string): Promise<void> {
    await this.usersRepository.delete(id);
  }

  async add(user: User): Promise<User> {
    return await this.usersRepository.save(user);
  }
}
