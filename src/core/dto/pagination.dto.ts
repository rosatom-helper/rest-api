import { IsInt, IsOptional } from 'class-validator';
import { Transform, Type } from 'class-transformer';
import {
  PAGINATION_DEFAULT_LIMIT,
  PAGINATION_DEFAULT_OFFSET,
} from '../consts/pagination.const';

export class PaginationDto {
  @IsOptional()
  @Type(() => Number)
  @IsInt()
  @Transform(parseInt)
  limit: number = PAGINATION_DEFAULT_LIMIT;

  @IsOptional()
  @Type(() => Number)
  @IsInt()
  @Transform(parseInt)
  offset: number = PAGINATION_DEFAULT_OFFSET;
}
