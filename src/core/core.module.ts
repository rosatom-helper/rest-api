import { Module, Global } from '@nestjs/common';
import { coreProviders } from './core.providers';

@Global()
@Module({
  imports: [],
  providers: [...coreProviders],
  exports: [...coreProviders],
})
export class CoreModule {}
