import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
} from '@nestjs/common';
import { httpStatusesConst } from '../consts/http-statuses.const';
import { ValidationError } from 'class-validator';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException & { message: any }, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const status = exception.getStatus();
    const error: { [key: string]: any } = {
      statusCode: status,
      message: httpStatusesConst[status],
      errors: [],
    };

    const { message, code = 1 } = exception.message;
    error.errorCode = code;

    if (message) {
      if (typeof message === 'string') {
        error.message = message;
      }

      if (message[0] instanceof ValidationError) {
        error.errorCode = 10;
        error.message = 'Validation failed';
        error.errors = message;
      }
    }

    if (process.env.DEBUG === 'true' || process.env.DEBUG === '1') {
      if (status >= 500 && typeof exception.stack === 'string') {
        error.stack = exception.stack.split('\n');
      }
    }

    response.status(status).json(error);
  }
}
