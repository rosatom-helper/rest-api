import { createConnection } from 'typeorm';
import { DATABASE_CONNECTION } from './consts/common.const';

export const databaseProviders = [
  {
    provide: DATABASE_CONNECTION,
    useFactory: async () => {
      return await createConnection({
        type: 'postgres',
        host:
          process.env.MODE === 'PRODUCTION'
            ? `/cloudsql/${process.env.INSTANCE_CONNECTION_NAME}`
            : 'localhost',
        username: process.env.SQL_USER,
        password: process.env.SQL_PASSWORD,
        database: process.env.SQL_DATABASE,
        entities: [__dirname + '/../../**/*.entity{.ts,.js}'],
        synchronize: true,
      });
    },
  },
];
