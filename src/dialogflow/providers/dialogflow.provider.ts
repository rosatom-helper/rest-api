import { Injectable } from '@nestjs/common';
import { TaskDto } from '@task/dto/task.dto';
import { TaskService } from '@task/services/task.service';
import { UsersService } from '@user/users.service';
import {
  DialogFlowFulfillmentResponse,
  DialogFlowIntent,
  DialogFlowParam,
} from 'nestjs-dialogflow';

@Injectable()
export class DialogFlowProvider {
  constructor(
    private taskService: TaskService,
    private usersService: UsersService,
  ) {}

  @DialogFlowIntent('Add card')
  public async handleAddingTheCard(
    @DialogFlowParam('queryResult.parameters') parameters,
  ): Promise<DialogFlowFulfillmentResponse> {
    const executorsIds = [];
    parameters.taskPerformers.forEach(async (performer) => {
      const user = await this.usersService.findByName(performer.name);
      if (user) {
        executorsIds.push(user.id);
      }
    });
    const task: TaskDto = {
      name: parameters.whatToExecute,
      endDate: parameters.deadline,
      executorsIds,
    };
    this.taskService.createTask(task);

    return {} as DialogFlowFulfillmentResponse;
  }
}
