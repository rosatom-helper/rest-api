import { Connection } from 'typeorm';
import { DATABASE_CONNECTION } from '@core/database/consts/common.const';
import { Role } from './entities/role.entity';
import { Permission } from './entities/permission.entity';
import {
  ROLE_REPOSITORY,
  PERMISSION_REPOSITORY,
} from './consts/database.const';

export const aclProviders = [
  {
    provide: ROLE_REPOSITORY,
    useFactory: (connection: Connection) => {
      return connection.getRepository(Role);
    },
    inject: [DATABASE_CONNECTION],
  },
  {
    provide: PERMISSION_REPOSITORY,
    useFactory: (connection: Connection) => {
      return connection.getRepository(Permission);
    },
    inject: [DATABASE_CONNECTION],
  },
];
