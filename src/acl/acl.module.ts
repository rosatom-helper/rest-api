import { Global, Module } from '@nestjs/common';
import { AclService } from './services/acl.service';
import { RoleService } from './services/role.service';
import { PermissionService } from './services/permission.service';
import { RoleController } from './controllers/role.controller';
import { PermissionController } from './controllers/permission.controller';
import { aclProviders } from './acl.providers';

@Global()
@Module({
  providers: [
    RoleService,
    PermissionService,
    {
      provide: 'acl',
      useClass: AclService,
    },
    ...aclProviders,
  ],
  exports: [
    RoleService,
    {
      provide: 'acl',
      useClass: AclService,
    },
    ...aclProviders,
  ],
  controllers: [RoleController, PermissionController],
})
export class AclModule {}
