import {
  ClassSerializerInterceptor,
  Controller,
  Get,
  Query,
  SerializeOptions,
  UseInterceptors,
} from '@nestjs/common';
import { PermissionService } from '../services/permission.service';
import { Permissions } from '../decorators';
import { PERMISSION } from '../consts/permissions.const';
import { Permission } from '../entities/permission.entity';
import { PermissionFilterDto } from '../dto/permission-filter.dto';

@Controller('permissions')
@UseInterceptors(ClassSerializerInterceptor)
export class PermissionController {
  constructor(private permissionService: PermissionService) {}

  @Get()
  @SerializeOptions({
    groups: ['default'],
  })
  @Permissions(PERMISSION.ROLES_VIEW)
  async getPermissions(
    @Query() params: PermissionFilterDto,
  ): Promise<Permission[]> {
    return this.permissionService.getPermissions(params);
  }
}
