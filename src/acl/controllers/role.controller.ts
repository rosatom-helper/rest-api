import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  Query,
  SerializeOptions,
  UseInterceptors,
} from '@nestjs/common';
import { RoleService } from '../services/role.service';
import { Role } from '../entities/role.entity';
import { RoleFilterDto } from '../dto/role-filter.dto';
import { NewRoleDto } from '../dto/new-role.dto';
import { UpdatedRoleDto } from '../dto/updated-role.dto';
import { Permissions } from '../decorators';
import { PERMISSION } from '../consts/permissions.const';

@Controller('roles')
@UseInterceptors(ClassSerializerInterceptor)
export class RoleController {
  constructor(private roleService: RoleService) {}

  @Get()
  @SerializeOptions({
    groups: ['default'],
  })
  @Permissions(PERMISSION.ROLES_VIEW)
  async getRoles(
    @Query() params: RoleFilterDto,
  ): Promise<{ roles: Role[]; total: number }> {
    return this.roleService.getRoles(params);
  }

  @Get(':id')
  @SerializeOptions({
    groups: ['default', 'permissions'],
  })
  @Permissions(PERMISSION.ROLES_VIEW)
  async getRole(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.roleService.getRole(id);
  }

  @Post()
  @SerializeOptions({
    groups: ['default'],
  })
  @Permissions(PERMISSION.ROLES_ADD)
  async createRole(@Body() newRoleDto: NewRoleDto): Promise<Partial<Role>> {
    return this.roleService.createRole(newRoleDto);
  }

  @Patch(':id')
  @SerializeOptions({
    groups: ['default', 'permissions'],
  })
  @Permissions(PERMISSION.ROLES_EDIT)
  async updateRole(
    @Param('id', new ParseUUIDPipe()) id: string,
    @Body() updatedRoleDto: UpdatedRoleDto,
  ): Promise<Partial<Role>> {
    return this.roleService.updateRole(id, updatedRoleDto);
  }

  @Delete(':id')
  @Permissions(PERMISSION.ROLES_DELETE)
  async deleteRole(
    @Param('id', new ParseUUIDPipe()) id: string,
  ): Promise<void> {
    return this.roleService.deleteRole(id);
  }

  @Patch(':id/activate')
  @Permissions(PERMISSION.ROLES_STATUS)
  async activateRole(
    @Param('id', new ParseUUIDPipe()) id: string,
  ): Promise<void> {
    await this.roleService.activateRole(id);
    return;
  }

  @Patch(':id/deactivate')
  @Permissions(PERMISSION.ROLES_STATUS)
  async deactivateRole(
    @Param('id', new ParseUUIDPipe()) id: string,
  ): Promise<void> {
    await this.roleService.deactivateRole(id);
    return;
  }
}
