import {
  IsBoolean,
  IsOptional,
  IsString,
  Matches,
  MaxLength,
  ValidateNested,
} from 'class-validator';
import { Transform, Type } from 'class-transformer';
import { PermissionDto } from './permission.dto';
import { Permission } from '../entities/permission.entity';

export class UpdatedRoleDto {
  @IsOptional()
  @Transform((value) => value.trim())
  @IsString()
  @MaxLength(255)
  @Matches(/^[a-z0-9- _]{1,255}$/i)
  name: string;

  @IsOptional()
  @Transform((value) => ['true', '1', true, 1].includes(value))
  @IsBoolean()
  isActive: boolean;

  @IsOptional()
  @ValidateNested()
  @Type(() => PermissionDto)
  permissions: Permission[];
}
