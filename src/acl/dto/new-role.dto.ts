import {
  IsOptional,
  IsString,
  IsBoolean,
  Matches,
  MaxLength,
  IsNotEmpty,
  ValidateNested,
} from 'class-validator';
import { Transform, Type } from 'class-transformer';
import { PermissionDto } from './permission.dto';

export class NewRoleDto {
  @IsNotEmpty()
  @Transform((value) => value.trim())
  @IsString()
  @MaxLength(255)
  @Matches(/^[a-z0-9- _]{1,255}$/i)
  name: string;

  @IsOptional()
  @Transform((value) => ['true', '1', true, 1].includes(value))
  @IsBoolean()
  isActive = true;

  @IsOptional()
  @ValidateNested()
  @Type(() => PermissionDto)
  permissions: PermissionDto[];
}
