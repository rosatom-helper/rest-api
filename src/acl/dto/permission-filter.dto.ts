import { IsInt, IsOptional, IsUUID } from 'class-validator';
import { Transform, Type } from 'class-transformer';

export class PermissionFilterDto {
  @IsOptional()
  @IsUUID()
  roleId: string;

  @IsOptional()
  @Type(() => Number)
  @IsInt()
  @Transform(parseInt)
  limit = 20;

  @IsOptional()
  @Type(() => Number)
  @IsInt()
  @Transform(parseInt)
  offset = 0;
}
