import {
  IsOptional,
  IsString,
  IsUUID,
  Matches,
  MaxLength,
} from 'class-validator';
import { Transform } from 'class-transformer';

export class RoleDto {
  @IsUUID()
  id: string;

  @IsOptional()
  @Transform((value) => value.trim())
  @IsString()
  @MaxLength(255)
  @Matches(/^[a-z0-9- _]{1,255}$/i)
  name: string;
}
