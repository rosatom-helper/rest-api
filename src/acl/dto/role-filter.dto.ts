import {
  IsOptional,
  IsString,
  IsIn,
  IsBoolean,
  Matches,
  MaxLength,
} from 'class-validator';
import { Transform } from 'class-transformer';
import { ASC, DESC } from '@core/consts/sort.const';
import { PaginationDto } from '@core/dto/pagination.dto';

export class RoleFilterDto extends PaginationDto {
  @IsOptional()
  @Transform((value) => ['true', '1'].includes(value))
  @IsBoolean()
  isActive: boolean;

  @IsOptional()
  @Transform((value) => value.trim())
  @IsString()
  @MaxLength(255)
  @Matches(/^[a-z0-9- _]{1,255}$/i)
  search: string;

  @IsOptional()
  @IsString()
  @IsIn([ASC, DESC])
  sort: string = DESC;
}
