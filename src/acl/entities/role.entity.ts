import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { Exclude, Expose } from 'class-transformer';
import { Permission } from './permission.entity';

export const SUPER_ADMIN = 'super_admin';
export const SIMPLE_USER = 'simple_user';

@Exclude()
@Entity('roles')
export class Role {
  @Expose({ groups: ['default'] })
  @PrimaryGeneratedColumn('uuid')
  readonly id: string;

  @Expose({ groups: ['default'] })
  @Column({
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  name: string;

  @Expose({ groups: ['default'] })
  @Column({
    type: 'varchar',
    length: 255,
    unique: true,
  })
  alias: string;

  @Expose({ groups: ['default'] })
  @Column({
    name: 'is_active',
    type: 'boolean',
    default: true,
  })
  isActive: boolean;

  @Expose({ groups: ['permissions'] })
  @ManyToMany(() => Permission, (permission) => permission.roles)
  @JoinTable({
    name: 'role_permissions',
    joinColumn: { name: 'permission_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'role_id', referencedColumnName: 'id' },
  })
  permissions: Permission[];

  @Expose({ groups: ['default'] })
  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @Expose({ groups: ['default'] })
  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;
}
