import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { Exclude, Expose } from 'class-transformer';
import { Role } from './role.entity';

@Exclude()
@Entity('permissions')
export class Permission {
  @Expose({ groups: ['default'] })
  @PrimaryGeneratedColumn('uuid')
  readonly id: string;

  @Expose({ groups: ['default'] })
  @Column({
    type: 'varchar',
    length: 255,
    unique: true,
  })
  name: string;

  @Expose({ groups: ['roles'] })
  @ManyToMany(() => Role, (role) => role.permissions)
  @JoinTable({
    name: 'role_permissions',
    joinColumn: { name: 'role_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'permission_id', referencedColumnName: 'id' },
  })
  roles: Role[];

  @Expose({ groups: ['default'] })
  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @Expose({ groups: ['default'] })
  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;
}
