import {
  Inject,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { Repository, ILike } from 'typeorm';
import { snakeCase } from '@core/utils';
import { Role, SIMPLE_USER, SUPER_ADMIN } from '../entities/role.entity';
import { RoleFilterDto } from '../dto/role-filter.dto';
import { NewRoleDto } from '../dto/new-role.dto';
import { UpdatedRoleDto } from '../dto/updated-role.dto';
import { Permission } from '../entities/permission.entity';
import {
  PERMISSION_REPOSITORY,
  ROLE_REPOSITORY,
} from '../consts/database.const';

@Injectable()
export class RoleService {
  constructor(
    @Inject(ROLE_REPOSITORY)
    private readonly roleRepository: Repository<Role>,
    @Inject(PERMISSION_REPOSITORY)
    private readonly permissionRepository: Repository<Permission>,
  ) {}

  async getRoles(
    roleFilterDto: RoleFilterDto,
  ): Promise<{ roles: Role[]; total: number }> {
    const options: { [key: string]: any } = {
      where: {},
      skip: roleFilterDto.offset,
      take: roleFilterDto.limit,
      order: {
        createdAt: roleFilterDto.sort,
      },
    };

    if (Object.prototype.hasOwnProperty.call(roleFilterDto, 'isActive')) {
      options.where.isActive = roleFilterDto.isActive;
    }

    if (roleFilterDto.search) {
      options.where.name = ILike(`%${roleFilterDto.search}%`);
    }

    try {
      const [roles, total] = await this.roleRepository.findAndCount(options);
      return { roles, total };
    } catch (e) {
      return {
        roles: [],
        total: 0,
      };
    }
  }

  async getRole(id: string): Promise<Role> {
    let role: Role = null;
    try {
      role = await this.roleRepository.findOne(id, {
        relations: ['permissions'],
      });
    } catch (e) {
      throw new InternalServerErrorException(e.message);
    }
    if (!role) {
      throw new NotFoundException('Role not found.');
    }
    return role;
  }

  async createRole(newRoleDto: NewRoleDto): Promise<Role> {
    let role: Role = null;

    try {
      role = this.roleRepository.create({
        name: newRoleDto.name,
        alias: snakeCase(newRoleDto.name),
        isActive: newRoleDto.isActive,
      });
      await this.roleRepository.save(role);
    } catch (e) {
      throw new InternalServerErrorException(e.message);
    }

    if (newRoleDto.permissions && newRoleDto.permissions.length) {
      try {
        const permissions = await this.permissionRepository.findByIds(
          newRoleDto.permissions.map(({ id }) => id),
        );

        role.permissions = permissions;
        await this.roleRepository.save(role);
      } catch (e) {
        throw new InternalServerErrorException(e.message);
      }
    }

    return role;
  }

  async updateRole(
    roleId: string,
    updatedRoleDto: Partial<Role> | UpdatedRoleDto,
  ) {
    const { permissions, ...updatedRole } = updatedRoleDto;
    let role: Role = null;

    try {
      role = await this.getRole(roleId);
    } catch (e) {
      throw e;
    }

    try {
      await this.roleRepository.update(roleId, updatedRole);
    } catch (e) {
      throw new InternalServerErrorException(e.message);
    }

    if (permissions && permissions.length) {
      try {
        const permissionsEntities = await this.permissionRepository.findByIds(
          permissions.map(({ id }) => id),
        );

        role.permissions = permissionsEntities;
        await this.roleRepository.save(role);
      } catch (e) {
        throw new InternalServerErrorException(e.message);
      }
    }

    return role;
  }

  async activateRole(id: string) {
    return this.updateRole(id, { isActive: true });
  }

  async deactivateRole(id: string) {
    return this.updateRole(id, { isActive: false });
  }

  async deleteRole(roleId: string) {
    let role;

    try {
      role = await this.roleRepository.findOne(roleId);
    } catch (e) {
      throw new InternalServerErrorException(e.message);
    }

    if ([SUPER_ADMIN, SIMPLE_USER].includes(role.alias)) {
      throw new NotFoundException('Role can not be removed.');
    }

    if (!role) {
      throw new NotFoundException('Role not found.');
    }

    try {
      await this.roleRepository.remove(role);
    } catch (e) {
      throw new InternalServerErrorException(e.message);
    }

    return;
  }
}
