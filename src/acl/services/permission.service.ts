import {
  Inject,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { Repository } from 'typeorm';
import { Permission } from '../entities/permission.entity';
import { PermissionFilterDto } from '../dto/permission-filter.dto';
import { PERMISSION_REPOSITORY } from '../consts/database.const';

@Injectable()
export class PermissionService {
  constructor(
    @Inject(PERMISSION_REPOSITORY)
    private readonly permissionRepository: Repository<Permission>,
  ) {}

  async getPermissions(params: PermissionFilterDto): Promise<Permission[]> {
    const options: { [key: string]: any } = {
      where: {},
      skip: params.offset,
      take: params.limit,
    };

    if (params.roleId) {
      options.join = {
        alias: 'permissions',
        innerJoin: {
          roles: 'permissions.roles',
        },
        where: (qb) => {
          qb.where('roles.id = :roleId', {
            roleId: params.roleId,
          });
        },
      };
    }

    try {
      return await this.permissionRepository.find(options);
    } catch (e) {
      throw new InternalServerErrorException(e.message);
    }
  }
}
