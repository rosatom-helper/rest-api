import { Inject, Injectable } from '@nestjs/common';
import { In, Repository } from 'typeorm';
import {
  ROLE_REPOSITORY,
  PERMISSION_REPOSITORY,
} from '../consts/database.const';
import { Role, SUPER_ADMIN } from '../entities/role.entity';
import { Permission } from '../entities/permission.entity';

@Injectable()
export class AclService {
  constructor(
    @Inject(ROLE_REPOSITORY)
    private readonly roleRepository: Repository<Role>,
    @Inject(PERMISSION_REPOSITORY)
    private readonly permissionRepository: Repository<Permission>,
  ) {}

  async hasRolesPermissions(roles: string[], permissions: string[]) {
    if (!roles.length || !permissions.length) {
      return false;
    }

    if (roles.includes(SUPER_ADMIN)) {
      return true;
    }

    const userPermissions = await this.getPermissionsByRoles(roles);

    return !!userPermissions.filter((permission) => {
      return permissions.includes(permission);
    }).length;
  }

  async getPermissionsByRoles(roleAliases: string[]): Promise<string[]> {
    let roles: Role[];

    try {
      roles = await this.roleRepository.find({
        where: {
          alias: In(roleAliases),
        },
        relations: ['permissions'],
      });
    } catch (e) {
      roles = [];
    }

    return roles.reduce((acc, { permissions }) => {
      return acc.concat(permissions.map(({ name }) => name));
    }, []);
  }
}
