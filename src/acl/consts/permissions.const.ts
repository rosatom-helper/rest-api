import { SIMPLE_USER } from '../entities/role.entity';

export const PERMISSION = {
  SIMPLE_USER,

  // DASHBOARD_VIEW: 'dashboard_view',
  // DASHBOARD_ADD: 'dashboard_add',
  // DASHBOARD_EDIT: 'dashboard_edit',
  // DASHBOARD_DELETE: 'dashboard_delete',
  // DASHBOARD_STATUS: 'dashboard_status',

  TASKS_VIEW: 'tasks_view',
  TASKS_ADD: 'tasks_add',
  TASKS_EDIT: 'tasks_edit',
  TASKS_DELETE: 'tasks_delete',

  SECTOR_VIEW: 'sector_view',
  SECTOR_ADD: 'sector_add',
  SECTOR_EDIT: 'sector_edit',
  SECTOR_DELETE: 'sector_delete',

  MANAGE_USERS_VIEW: 'manage_users_view',
  MANAGE_USERS_ADD: 'manage_users_add',
  MANAGE_USERS_EDIT: 'manage_users_edit',
  MANAGE_USERS_DELETE: 'manage_users_delete',

  ROLES_VIEW: 'roles_view',
  ROLES_ADD: 'roles_add',
  ROLES_EDIT: 'roles_edit',
  ROLES_DELETE: 'roles_delete',
  ROLES_STATUS: 'roles_status',

  REPORTS_VIEW: 'reports_view',
};
